import React from "react";

const Banner = () => {
  return (
    <div className="w-full">
      <img
        className="w-full h-[400px] w-full object-cover"
        src="https://cdn.shopify.com/s/files/1/1903/4853/files/slideshow3-home2_1920x.jpg?v=1613166679"
        alt="Img"
      />
    </div>
  );
};

export default Banner;
