import React, { useEffect, useState } from "react";
import { apiGetProducts } from "../api/product";
// import { Product } from "../../components";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Product from "./product";
import { useDispatch, useSelector } from "react-redux";
import { getNewProduct } from "../store/products/asyncActions";

const tabs = [
  {
    id: 1,
    name: "best sellers",
  },
  {
    id: 2,
    name: "new arrivals",
  },
];

var settings = {
  pauseOnHover: false,
  infinite: true,
  loop: true,
  dots: false,
  infinite: false,
  speed: 3000,
  slidesToShow: 3,
  slidesToScroll: 1,
  //   autoplay: true,
};

const BestSeller = () => {
  const [bestSeller, setBestSeller] = useState(null);
  const [activesTab, setActivesTab] = useState(1);
  const [product, setProduct] = useState(null);

  const dispatch = useDispatch();

  const { newProducts } = useSelector((state) => state.products);

  const fetchProduct = async () => {
    const response = await apiGetProducts({ sort: "-sold" });
    if (response?.data?.success) {
      setBestSeller(response.data?.products);
      setProduct(response.data?.products);
    }
  };

  useEffect(() => {
    fetchProduct();
    dispatch(getNewProduct());
  }, []);

  useEffect(() => {
    if (activesTab === 1) setProduct(bestSeller);
    if (activesTab === 2) setProduct(newProducts);
  }, [activesTab]);

  return (
    <div>
      <div className="flex text-[20px] gap-8 pb-4 border-b-2 border-main">
        {tabs.map((el,index) => (
          <span
            key={index}
            className={`font-semibold capitalize cursor-pointer ${
              activesTab === el.id ? "text-black" : "text-gray-400"
            }`}
            onClick={() => setActivesTab(el.id)}
          >
            {el.name}
          </span>
        ))}
      </div>
      <div className="mt-4 mx-[-10px]">
        <Slider {...settings}>
          {bestSeller?.map((el,index) => (
            <Product key={index} data={el} pid={el}></Product>
          ))}
        </Slider>
      </div>
      <div className="w-full flex gap-4 mt-4">
        <img
          className="flex-1 object-contain"
          src="https://cdn.shopify.com/s/files/1/1903/4853/files/banner1-home2_2000x_crop_center.png?v=1613166657"
          alt="img"
        />
        <img
          src="https://cdn.shopify.com/s/files/1/1903/4853/files/banner2-home2_2000x_crop_center.png?v=1613166657"
          alt="Img"
        />
      </div>
    </div>
  );
};

export default BestSeller;
