import React from "react";
import useBreadcrumbs from "use-react-router-breadcrumbs";
import { Link } from "react-router-dom";

const Breadcrumbs = ({ title, category }) => {
  const routes = [
    { path: "/:category", breadcrumb: category },
    { path: "/", breadcrumb: "Home" },
    { path: "/:category/:pid/:title", breadcrumb: title },
  ];

  const breadcrumb = useBreadcrumbs(routes);

  return (
    <div className="text-sm pt-[8px]">
      {breadcrumb
        ?.filter((el) => !el.match.route === false)
        .map(({ match, breadcrumb }, index, self) => (
          <Link key={match.pathname} to={match.pathname}>
            <span className="hover:text-main"> {breadcrumb} </span>
            <span>{index !== self.length - 1 && <small>{">"}</small>}</span>
          </Link>
        ))}
    </div>
  );
};

export default Breadcrumbs;
