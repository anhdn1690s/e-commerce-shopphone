import React from "react";

const Button = ({ name, handleOnclick, style }) => {
  return (
    <button
      type="button"
      className={style ? style : "border bg-gray-500 px-[10px] py-[5px]"}
      onClick={() => {
        handleOnclick && handleOnclick();
      }}
    >
      {name}
    </button>
  );
};

export default Button;
