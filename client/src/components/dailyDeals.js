import React, { useEffect, useState } from "react";
import icons from "../ultils/icons";
import { apiGetProducts } from "../api/product";
import { formatMoney, renderStartFromNumber } from "../ultils/helpers";
import CountDown from "./countDown";

const DailyDeals = () => {
  const { AiFillStar, AiOutlineMenu } = icons;
  const [dealDaily, setDealDaily] = useState(null);
  const [hours, setHours] = useState(0);
  const [minute, setMinute] = useState(0);
  const [seconds, setSeconds] = useState(0);
  const fetchDealDaily = async () => {
    const response = await apiGetProducts({
      limit: 1,
      page: 5,
      totalRatings: 5,
    });
    if (response?.data?.success) setDealDaily(response?.data?.products[0]);
  };
  useEffect(() => {
    fetchDealDaily();
  }, []);

  return (
    <div className="w-full border py-[10px] px-[15px]">
      <div className="flex items-center justify-between">
        <span className="flex-1">
          <AiFillStar color="#d11" />
        </span>
        <span className="flex-1">DAILY DEALS</span>
        <span className="flex-1"></span>
      </div>
      <div className="w-full flex flex-col items-center pt-8 gap-2">
        <img
          src={
            dealDaily?.thumb ||
            "https://annatabeachhotel.vn/wp-content/uploads/woocommerce-placeholder.png"
          }
          alt="Img"
          className="w-full object-contain"
        ></img>
        <span className="line-clamp-1">{dealDaily?.title}</span>
        <span className="flex gap-1">
          {renderStartFromNumber(dealDaily?.totalRatings)?.map((el, index) => (
            <span key={index}>{el}</span>
          ))}
        </span>
        <span>{`${formatMoney(dealDaily?.price)} VND`}</span>
      </div>
      <div className="px-4 mt-4">
        <div className="flex justify-center gap-2 items-center pb-[10px]">
          <CountDown unit={"Hours"} number={hours} />
          <CountDown unit={"Minute"} number={minute} />
          <CountDown unit={"Seconds"} number={seconds} />
        </div>
        <button
          type="button"
          className="flex gap-2 items-center justify-center w-full hover:bg-gray-800 text-white py-2 bg-main"
        >
          <AiOutlineMenu />
          Option
        </button>
      </div>
    </div>
  );
};

export default DailyDeals;
