import React from 'react'

const Footer = () => {
  return (
    <div className='w-full bg-gray-700 flex justify-center pt-[50px] pb-[50px] text-gray-100'>Copyright ©2023 All rights reserved by @anhdn</div>
  )
}

export default Footer