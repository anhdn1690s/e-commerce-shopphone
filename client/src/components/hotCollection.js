import React from "react";
import { useSelector } from "react-redux";

const HotCollection = () => {
  // const { newProducts } = useSelector((state) => state.products);
  const { categories } = useSelector((state) => state.app);

  // console.log("data", newProducts);
  // console.log("cate", categories);

  return (
    <div className="w-full">
      <h3 className="text-[20px] py-[15px] border-b-2 border-main">
        HOT COLLECTIONS{" "}
      </h3>
      <div className="flex flex-wrap gap-4 mt-4 ">
        {categories
          ?.filter((el) => el.brand.length > 0)
          ?.map((el, index) => (
            <div key={index} className="w-[396px] ">
              <div className="border flex p-4 gap-4 min-h-[202px]">
                <img
                  src={el.image}
                  alt="Img"
                  className="flex-1 w-[144px] h-[129px] w-full object-cover"
                ></img>
                <div className="flex-1 text-gray-700">
                  <h4 className="text-semibold uppercase">{el.title}</h4>
                  <ul className="text-sm ">
                    {el?.brand?.map((brand) => (
                      <li key={brand} className="hover:text-main ">
                        {brand}
                      </li>
                    ))}
                  </ul>
                </div>
              </div>
            </div>
          ))}
      </div>
    </div>
  );
};

export default HotCollection;
