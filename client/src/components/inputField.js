import React from "react";

const InputField = ({
  value,
  setValue,
  nameKey,
  type,
  register,
  error,
  invalidFields,
  setInvalidFields,
}) => {
  return (
    <div>
      <label htmlFor={nameKey} className="pr-[10px]">
        {nameKey.slice(0, 1).toUpperCase() + nameKey.slice(1)}
      </label>
      <input
        className="border mb-[10px]"
        {...register(nameKey, { required: true })}
        type={type || "text"}
        placeholder={nameKey.slice(0, 1).toUpperCase() + nameKey.slice(1)}
        value={value}
        onChange={(e) =>
          setValue((pre) => ({ ...pre, [nameKey]: e.target.value }))
        }
        // onFocus={() => setInvalidFields([])}
      ></input>
      {error[nameKey] && <small>{nameKey} is required</small>}
      {/* {invalidFields?.some((el) => el.name === nameKey) && 
        <small>{invalidFields.find((el) => el.name === nameKey)?.mes}</small>
      } */}
    </div>
  );
};

export default InputField;
