import React from "react";

const PostBlog = () => {
  return (
    <div className="w-full">
      <h3 className="text-[20px] py-[15px] border-b-2 border-main">
        BLOG POSTS{" "}
      </h3>
      <div className="h-[300px] bg-main"></div>
    </div>
  );
};

export default PostBlog;
