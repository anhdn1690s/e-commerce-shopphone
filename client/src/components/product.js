import React, { useState } from "react";
import { formatMoney, renderStartFromNumber } from "../ultils/helpers";
import SelectOption from "./selectOption";
import icons from "../ultils/icons";
import { Link } from "react-router-dom";
import path from "../ultils/path";
const Product = ({ data }) => {
  const { AiFillEye, AiOutlineMenu, BsFillSuitHeartFill } = icons;
  const [isShowOption, setIsShowOption] = useState(false);
  return (
    <div className="px-[10px]">
      <Link
        to={`/${data?.category.toLowerCase()}/${data._id}/${data.title
          .toLowerCase()
          .replace(/\s/g, "-")}`}
        className="w-full border p-[15px] flex flex-col items-center"
        onMouseEnter={(e) => {
          e.stopPropagation();
          setIsShowOption(true);
        }}
        onMouseLeave={(e) => {
          e.stopPropagation();
          setIsShowOption(false);
        }}
      >
        <div className="w-full relative">
          {isShowOption && (
            <div className="absolute bottom-0 left-0 right-0 flex justify-center gap-3 animate-slide-top">
              <SelectOption icon={<AiFillEye />}></SelectOption>
              <SelectOption icon={<AiOutlineMenu />}></SelectOption>
              <SelectOption icon={<BsFillSuitHeartFill />}></SelectOption>
            </div>
          )}
          <img
            src={
              data?.thumb ||
              "https://annatabeachhotel.vn/wp-content/uploads/woocommerce-placeholder.png"
            }
            alt="Img"
            className="w-[243px] h-[243px] object-cover"
          ></img>
        </div>
        <div className="flex flex-col gap-2 mt-[15px] items-start w-full gap-2">
          <span className="line-clamp-1">{data?.title}</span>
          <span className="flex gap-1">
            {renderStartFromNumber(data?.totalRatings)?.map((el,index) => (
              <span key={index}>{el}</span>
            ))}
          </span>
          <span>{`${formatMoney(data?.price)} VND`}</span>
        </div>
      </Link>
    </div>
  );
};

export default Product;
