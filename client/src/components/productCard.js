import React from "react";
import { formatMoney, renderStartFromNumber } from "../ultils/helpers";
const ProductCard = ({ data }) => {
  return (
    <div className="w-1/3 flex-auto flex px-[10px] pb-[20px]">
      <div className="flex w-full border">
        <img
          src={data?.thumb}
          alt="Img"
          className="w-[120px] object-contain p-4"
        ></img>
        <div className="flex flex-col gap-2 mt-[15px] items-start w-full">
          <span className="line-clamp-1 text-sm">{data?.title}</span>
          <span className="flex h-4">
            {renderStartFromNumber(data?.totalRatings, 14)?.map((el,index) => (
              <span key={index}>{el}</span>
            ))}
          </span>
          <span>{`${formatMoney(data?.price)} VND`}</span>
        </div>
      </div>
    </div>
  );
};

export default ProductCard;
