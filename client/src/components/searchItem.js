import React, { memo, useState, useEffect, useCallback } from "react";
import icons from "../ultils/icons";
import { color } from "../ultils/contants";
import { createSearchParams, useNavigate, useParams } from "react-router-dom";
import { apiGetProducts } from "../api/product";

import { formatMoney } from "../ultils/helpers";

const SearchItem = ({
  name,
  activeClick,
  ChangeActiveFilter,
  type = "checkbox",
}) => {
  const navigation = useNavigate();
  const { category } = useParams();

  const { AiOutlineDown } = icons;
  const [selected, setSelected] = useState([]);
  // const [bestPrice, setBestPrice] = useState(null);

  const handleSelected = (e) => {
    const alreadyEl = selected.find((el) => el === e.target.value);
    if (alreadyEl) {
      setSelected((prev) => prev.filter((el) => el !== e.target.value));
    } else {
      setSelected((prev) => [...prev, e.target.value]);
    }

    ChangeActiveFilter(null);
  };

  // const fecthPriceProduct = async () => {
  //   const response = await apiGetProducts({ sort: "-price", limit: 1 });
  //   if (response?.data?.success) {
  //     // console.log(response);
  //     setBestPrice(response?.data?.products[0].price);
  //   }
  // };

  useEffect(() => {
    if (selected.length > 0) {
      navigation({
        pathname: `/${category}`,
        search: createSearchParams({
          color: selected.join(","),
        }).toString(),
      });
    } else {
      navigation(`/${category}`);
    }
  }, [category, selected, navigation]);

  // useEffect(() => {
  //   if (type === "input") {
  //     fecthPriceProduct();
  //   }
  // }, [type]);



  return (
    <div
      onClick={() => ChangeActiveFilter(name)}
      className="p-4 text-xs border border-gray-800 flex justify-between items-center relative gap-6"
    >
      <span>{name} </span>
      <AiOutlineDown />
      {activeClick === name && (
        <div className="absolute top-full left-0 w-fit border z-10 bg-white mt-1 p-4">
          {type === "checkbox" && (
            <>
              <div className=" items-center flex justify-between text-gray-700 gap-8 min-w-[150px] ">
                <span>{`${selected.length} selected`}</span>
                <span
                  className="underline hover:text-main cursor-pointer"
                  onClick={(e) => {
                    e.stopPropagation();
                    setSelected([]);
                  }}
                >
                  Reset
                </span>
              </div>
              <div
                className="flex flex-col gap-3 mt-5"
                onClick={(e) => e.stopPropagation()}
              >
                {color.map((el) => (
                  <div key={el} className="flex gap-2">
                    <input
                      type="checkbox"
                      name={el}
                      onChange={handleSelected}
                      value={el}
                      id={el}
                      checked={selected.some((selected) => selected === el)}
                    />
                    <label htmlFor={el}>{el}</label>
                  </div>
                ))}
              </div>
            </>
          )}
          {/* {type === "input" && (
            <>
              <div className=" items-center flex justify-between text-gray-700 gap-8 min-w-[350px] ">
                <span>{`The highest price is ${formatMoney(
                  bestPrice
                )} VND`}</span>
                <span
                  className="underline hover:text-main cursor-pointer"
                  onClick={(e) => {
                    e.stopPropagation();
                    setSelected([]);
                  }}
                >
                  Reset
                </span>
              </div>
              <div className="flex items-center p-2 gap-2">
                <div className="flex items-center gap-2">
                  <label htmlFor="from">From</label>
                  <input
                    className="from-input border p-2"
                    type="number"
                    id="from"
                    value={price[0]}
                    onChange={(e) =>
                      setPrice((prev) => ({ ...prev, from: e.target.value }))
                    }
                  />
                </div>
                <div className="flex items-center gap-2">
                  <label htmlFor="from">From</label>
                  <input
                    className="from-input border p-2"
                    type="number"
                    id="to"
                    value={price[1]}
                    onChange={(e) =>
                      setPrice((prev) => [...prev, e.target.value])
                    }
                  />
                </div>
              </div>
            </>
          )} */}
        </div>
      )}
    </div>
  );
};

export default memo(SearchItem);
