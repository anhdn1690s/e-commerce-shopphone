import React, { memo } from "react";

const SelectQuantity = ({
  quantity,
  handleQuantity,
  handleOnChangeQuantity,
}) => {
  return (
    <div className="flex items-center">
      <span
        className="text-[16px] p-2 border-r border-black cursor-pointer"
        onClick={() => handleOnChangeQuantity("minus")}
      >
        -
      </span>
      <input
        type="text"
        className="py-2 outline-none w-[50px] text-center"
        value={quantity}
        onChange={(e) => handleQuantity(e.target.value)}
      ></input>

      <span
        className="text-[16px] p-2 border-l border-black cursor-pointer"
        onClick={() => handleOnChangeQuantity("plus")}
      >
        +
      </span>
    </div>
  );
};

export default memo(SelectQuantity);
