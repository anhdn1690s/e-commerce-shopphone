import React, { useEffect, useState } from "react";
import { apiGetCategory } from "../api/app";
import { NavLink } from "react-router-dom";
import { createSlug } from "../ultils/helpers";
import { useSelector } from "react-redux";

const Sidebar = () => {
  const { categories } = useSelector((state) => state.app);
  // console.log("cate", categories);

  return (
    <div className="flex flex-col border">
      {categories?.map((item, index) => (
        <NavLink
          to={createSlug(item.title)}
          key={index}
          className={({ active }) =>
            active
              ? "bg-main hover:text-main px-15 pt-[15px] pb-[15px]"
              : "pl-[15px] pt-[15px] pb-[15px] text-sm hover:text-main "
          }
        >
          {item.title}
        </NavLink>
      ))}
    </div>
  );
};

export default Sidebar;
