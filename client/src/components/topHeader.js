import React, { useEffect } from "react";
import { Link } from "react-router-dom";
import path from "../ultils/path";
import { useDispatch, useSelector } from "react-redux";
import { getCurrent } from "../store/user/asyncActions";
import icons from "../ultils/icons";
import { logout } from "../store/user/userSlice";
const TopHeader = () => {
  const { AiOutlineLogout } = icons;
  const dispatch = useDispatch();
  const { isLoggedIn, current } = useSelector((state) => state.user);
  useEffect(() => {
    if (isLoggedIn) {
      dispatch(getCurrent());
    }
  }, [dispatch, isLoggedIn]);

  return (
    <div className="w-full h-[38px] bg-main flex items-center justify-center">
      <div className="w-main flex items-center justify-center justify-between text-xs text-white">
        <span>ORDER ONLINE OR CALL US (+1800) 000 8808</span>
        {isLoggedIn ? (
          <small className="flex items-center gap-2 text-xs">
            <span>{`Hello, ${current?.lastname} ${current?.firstname}`}</span>
            <span
              className="hover:rounded-full hover:bg-gray-200 hover:text-main p-1 cursor-pointer"
              onClick={() => {
                dispatch(logout());
              }}
            >
              <AiOutlineLogout size={18} />
            </span>
          </small>
        ) : (
          <Link to={`/${path.LOGIN}`}>Sign In or Create Account</Link>
        )}
      </div>
    </div>
  );
};

export default TopHeader;
