import React, { useCallback, useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { apiProducts } from "../../api/product";
import {
  Breadcrumbs,
  Button,
  Extrainfo,
  Information,
  SelectQuantity,
} from "../../components";
import { formatMoney, renderStartFromNumber } from "../../ultils/helpers";

const DetailsProduct = () => {
  const { pid } = useParams();
  const [product, setProduct] = useState(null);
  const [quantity, setQuantity] = useState(1);
  const fecthDetails = async () => {
    const response = await apiProducts(pid);
    if (response?.data?.success) {
      setProduct(response);
      console.log("data", product);
    }
  };
  useEffect(() => {
    if (pid) {
      fecthDetails();
    }
  }, [pid]);

  const handleQuantity = useCallback(
    (number) => {
      if (!Number(number) || Number(number) < 1) {
        return;
      } else {
        setQuantity(number);
      }
    },
    [quantity]
  );
  const handleOnChangeQuantity = useCallback(
    (flag) => {
      if (flag === "minus" && quantity === 1) return;
      if (flag === "minus") setQuantity((prev) => prev - 1);
      if (flag === "plus") setQuantity((prev) => prev + 1);
    },
    [quantity]
  );

  return (
    <div className="w-full">
      <div className="h-[81px] bg-gray-100 flex justify-center items-center">
        <div className="w-main">
          <h3>{product?.data?.productData?.title}</h3>
          <Breadcrumbs
            title={product?.data?.productData?.title}
            category={product?.data?.productData?.category}
          />
        </div>
      </div>
      <div className="w-main m-auto mt-4 flex">
        <div className="flex flex-col gap-4 w-2/5">
          <img
            src={product?.data?.productData?.images}
            alt="Img"
            className="h-[450px] w-[450px] border object-cover"
          />
        </div>
        <div className=" w-2/5 flex flex-col">
          <h2 className="text-[30px] front-semibold">{`${formatMoney(
            product?.data?.productData?.price
          )} VND`}</h2>
          <div className="flex mt-4">
            {renderStartFromNumber(
              product?.data?.productData?.totalRatings
            )?.map((el,index) => (
              <span key={index}>{el}</span>
            ))}
          </div>
          <ul className="mt-4  text-sm ml-[15px] ">
            {product?.data?.productData?.description.map((el) => (
              <li key={el} className="list-disc mb-[3px] text-gray-500">
                {el}
              </li>
            ))}
          </ul>
          <div className="flex items-center mt-5 gap-5">
            <span>Quantity</span>

            <SelectQuantity
              quantity={quantity}
              handleQuantity={handleQuantity}
              handleOnChangeQuantity={handleOnChangeQuantity}
            />
          </div>
          <Button
            name={"Add To Cart"}
            style={"bg-main p-2 mt-[20px] text-gray-100"}
          />
        </div>
        <div className=" w-1/5">
          <Extrainfo />
        </div>
      </div>
      <div className="w-main  m-auto mt-20 mb-20">
        <Information />
      </div>
    </div>
  );
};

export default DetailsProduct;
