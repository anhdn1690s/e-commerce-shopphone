import React, { useEffect } from "react";
import { useParams, useNavigate } from "react-router-dom";
import path from "../../ultils/path";
import Swal from "sweetalert2";

const FinalRegister = () => {
  const { status } = useParams();
  const navigate = useNavigate();
  useEffect(() => {
    if (status === "failed")
      Swal.fire("Oop!", "Dang ky khong thanh cong", "error").then(() => {
        navigate(`/${path.LOGIN}`);
      });
    if (status === "success")
      Swal.fire(
        "Congratulation",
        "Dang ky thanh cong.hay dang nhap",
        "success"
      ).then(() => {
        navigate(`/${path.LOGIN}`);
      });
  }, []);

  return <div>hihi</div>;
};

export default FinalRegister;
