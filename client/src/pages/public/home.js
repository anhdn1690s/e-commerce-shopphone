import React from "react";
import {
  Banner,
  Sidebar,
  BestSeller,
  DailyDeals,
  FeaturedProduct,
  HotCollection,
  PostBlog,
} from "../../components";
const Home = () => {
  return (
    <>
      <div className="w-main flex">
        <div className="flex flex-col gap-5 w-[25%] flex-auto ">
          <Sidebar></Sidebar>
          <DailyDeals />
        </div>
        <div className="flex flex-col gap-5 pl-5 w-[75%] flex-auto">
          <Banner></Banner>
          <BestSeller />
        </div>
      </div>
      <div className="w-main my-8">
        <FeaturedProduct />
      </div>
      <div className="w-main my-8">
        <HotCollection />
      </div>
      <div className="w-main my-8">
        <PostBlog />
      </div>
    </>
  );
};

export default Home;
