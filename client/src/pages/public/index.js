export { default as Home } from "./home";
export { default as Login } from "./login";
export { default as Public } from "./public";
export { default as Products } from "./products";
export { default as DetailsProduct } from "./detailsProduct";
export { default as Blogs } from "./blogs";
export { default as FinalRegister } from "./finalRegister";
export { default as ResetPassword } from "./resetPassword";
