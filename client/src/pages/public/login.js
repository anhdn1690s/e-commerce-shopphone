import React, { useCallback, useState, useEffect } from "react";
import { Button, InputField } from "../../components";
import {
  apiFinalRegister,
  apiForgotPassword,
  apiLogin,
  apiRegister,
} from "../../api/user";
import Swal from "sweetalert2";
import { useNavigate } from "react-router-dom";
import path from "../../ultils/path";
import { useDispatch } from "react-redux";
import { login } from "../../store/user/userSlice";
import { toast } from "react-toastify";
import { useForm } from "react-hook-form";

const Login = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const {
    register,
    handleSubmit,
    formState: { isValid, errors, isSubmitting },
  } = useForm();

  const [payload, setPayload] = useState({
    email: "",
    password: "",
    firstname: "",
    lastname: "",
    mobile: "",
  });
  const [token, setToken] = useState("");
  const [isVeriFideEmail, setIsVeriFideEmail] = useState(false);
  const [isRegister, setIsRegister] = useState(false);
  const [isForgotPassword, setIsForgotPassword] = useState(false);
  const [emailForgotPassword, setEmailForgotPassword] = useState("");

  const resetPayload = () => {
    setPayload({
      email: "",
      password: "",
      firstname: "",
      lastname: "",
      mobile: "",
    });
  };

  useEffect(() => {
    resetPayload();
  }, [isRegister]);

  const handleButtonSubmit = useCallback(async () => {
    const { firstname, lastname, ...data } = payload;
    if (isRegister) {
      const response = await apiRegister(payload);
      if (response?.data?.success) {
        setIsVeriFideEmail(true);
      } else {
        Swal.fire("Oops!", response?.data?.mes, "Error");
      }
    } else {
      const responseLogin = await apiLogin(data);
      if (responseLogin?.data.sucess) {
        dispatch(
          login({
            isLoggedIn: true,
            token: responseLogin?.data?.accessToken,
            userData: responseLogin?.data?.userData,
          })
        );
        navigate(`/${path.HOME}`);
      } else {
        Swal.fire("Oops!", responseLogin?.data?.mes, "Error");
      }
    }
    // }
  }, [payload, isRegister, dispatch, navigate]);

  const handleForgotPassword = async () => {
    const response = await apiForgotPassword(emailForgotPassword);
    if (response.data.success) {
      toast.success(response.data.mes);
    } else {
      toast.info(response.data.mes);
    }
  };

  const finalRegister = async () => {
    console.log(token);
    const response = await apiFinalRegister(token);
    console.log(response);
    if (response?.data?.success) {
      Swal.fire("Congratulation", response?.data?.mes, "Successfully").then(
        () => setIsRegister(false),
        resetPayload()
      );
    } else {
      Swal.fire("Oops!", response?.data?.mes, "Error");
    }
    setIsVeriFideEmail(false);
    setToken("");
  };

  return (
    <div className="px-[20px] py-[20px] relative">
      {isVeriFideEmail && (
        <div className="absolute top-0 left-0 bottom-0 right-0 bg-overlay z-50 h-[100vh]">
          <div className="bg-white h-[150px] w-[200px] grid mx-[auto] my-[0]">
            <p>We sent a code to your mail</p>
            <input
              value={token}
              type="text"
              onChange={(e) => setToken(e.target.value)}
            />
            <button
              type="button"
              className="px-[20px] border bg-gray-500 text-white"
              onClick={() => {
                finalRegister();
              }}
            >
              {" "}
              Submit
            </button>
          </div>
        </div>
      )}
      {isForgotPassword && (
        <div className="absolute top-0 left-0 bottom-0 right-0 bg-overlay z-50 h-[100vh]">
          <div className="bg-white h-[150px] w-[200px] grid mx-[auto] my-[0]">
            <p>ForgotPassword</p>
            <InputField
              value={emailForgotPassword.email}
              setValue={setEmailForgotPassword}
              nameKey="emailForgot"
              register={register}
              error={errors}
            />

            <Button
              name={"ForgotPassword"}
              handleOnclick={() => {
                handleForgotPassword();
              }}
            />
            <Button
              name={"Back"}
              handleOnclick={() => {
                setIsForgotPassword(false);
              }}
            />
          </div>
        </div>
      )}

      <p className="pb-[10px]">{isRegister ? "Register" : "Login!!!"}</p>
      <form onSubmit={handleSubmit(handleButtonSubmit)}>
        {isRegister && (
          <>
            <InputField
              value={payload.firstname}
              setValue={setPayload}
              nameKey="firstname"
              register={register}
              error={errors}
            />
            <InputField
              value={payload.lastname}
              setValue={setPayload}
              nameKey="lastname"
              register={register}
              error={errors}
            />
            <InputField
              value={payload.mobile}
              setValue={setPayload}
              nameKey="mobile"
              register={register}
              error={errors}
            />
          </>
        )}

        <InputField
          value={payload.email}
          setValue={setPayload}
          nameKey="email"
          register={register}
          error={errors}
        />
        <InputField
          value={payload.password}
          setValue={setPayload}
          nameKey="password"
          type="password"
          register={register}
          error={errors}
        />
        <button
          type="submit"
          className="px-[20px] border bg-gray-500 text-white"
          // disabled={!isValid}
        >
          {isSubmitting ? (
            <span>Processing...</span>
          ) : (
            <span>{isRegister ? "Register" : "Login"}</span>
          )}
        </button>
        {/* <Button
          name={isRegister ? "Register" : "Login"}
          handleOnclick={() => handleButtonSubmit()}
        /> */}
      </form>

      {isRegister ? (
        <p onClick={() => setIsRegister(false)}>go to login!!</p>
      ) : (
        <p onClick={() => setIsRegister(true)}>Create Account!!</p>
      )}

      {isRegister ? (
        ""
      ) : (
        <p
          onClick={() => {
            setIsForgotPassword(true);
          }}
        >
          Forgot your password?
        </p>
      )}
    </div>
  );
};

export default Login;
