import React, { useEffect, useState, useCallback } from "react";
import { useParams, useSearchParams } from "react-router-dom";
import { Breadcrumbs, Product, SearchItem } from "../../components";
import { apiGetProducts } from "../../api/product";
import { color } from "../../ultils/contants";

const Products = () => {
  const { category } = useParams();

  const [productsByCate, setProductsByCate] = useState(null);
  const [activeClick, setActiveClick] = useState(null);
  const [params] = useSearchParams();

  const fecthProduct = async (queries) => {
    const response = await apiGetProducts(queries);
    if (response?.data?.success) {
      setProductsByCate(response);
    }
  };

  useEffect(() => {
    let param = [];
    for (let i of params.entries()) param.push(i);
    const queries = {};
    for (let i of params) queries[i[0]] = i[1];
    fecthProduct(queries);
  }, [params]);

  const ChangeActiveFilter = useCallback((name) => {
    if (activeClick === name) setActiveClick(null);
    else setActiveClick(name);
  }, []);

  // console.log(productsByCate);

  return (
    <div className="w-full">
      <div className="h-[81px] bg-gray-100 flex justify-center items-center">
        <div className="w-main">
          <h3>{category.toUpperCase()}</h3>
          <Breadcrumbs category={category} />
        </div>
      </div>
      <div className="w-main m-auto border p-4 flex justify-between mt-10">
        <div className="w-4/5 flex-auto flex items-center gap-4">
          {/* <SearchItem
            name="Price"
            activeClick={activeClick}
            ChangeActiveFilter={ChangeActiveFilter}
            type="input"
          /> */}
          <SearchItem
            name="Color"
            activeClick={activeClick}
            ChangeActiveFilter={ChangeActiveFilter}
          />
        </div>
        <div className="w-1/5 flex-auto">Filter</div>
      </div>
      <div className="w-main my-0 mx-auto flex flex-wrap mt-[15px] mx-[-10px] justify-between">
        {productsByCate?.data?.products?.map((el) => (
          <Product key={el._id} data={el} />
        ))}
      </div>
    </div>
  );
};

export default Products;
