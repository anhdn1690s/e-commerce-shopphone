import React, { useState } from "react";
import { Button, InputField } from "../../components";
import { useParams } from "react-router-dom";
import { apiResetPassword } from "../../api/user";
import { toast } from "react-toastify";

const ResetPassword = () => {
  const [password, setPassword] = useState("");

  const { token } = useParams();

  const handleResetPassword = async () => {
    // console.log({ token, resetPassword });
    const response = await apiResetPassword({ password, token });
    // console.log(response);
    if (response.data.success) {
      toast.success(response.data.mes);
    } else {
      toast.info(response.data.mes);
    }
  };
  return (
    <div className="absolute top-0 left-0 bottom-0 right-0 bg-overlay z-50 h-[100vh]">
      <div className="bg-white h-[150px] w-[200px] grid mx-[auto] my-[0]">
        <p>Reset Password</p>
        {/* <InputField
          value={resetPassword}
          setValue={setResetPassword}
          nameKey="password"
        //   type="password"
        /> */}
        <input
          id="password"
          value={password}
          type="text"
          onChange={(e) => setPassword(e.target.value)}
        />
        <Button
          name={"ForgotPassword"}
          handleOnclick={() => {
            handleResetPassword();
          }}
        />
      </div>
    </div>
  );
};

export default ResetPassword;
