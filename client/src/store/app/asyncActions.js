import { createAsyncThunk } from "@reduxjs/toolkit";
import * as api from "../../api";

export const getCategories = createAsyncThunk(
  "app/categories",
  async (data, { rejectWithValue }) => {
    const response = await api.apiGetCategory();
    // console.log("data", response.data)
    if (!response.data.success) return rejectWithValue(response);
    return response.data.productCategory;
  }
);
