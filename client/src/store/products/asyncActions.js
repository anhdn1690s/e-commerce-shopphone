import { createAsyncThunk } from "@reduxjs/toolkit";
import { apiGetProducts } from "../../api/product";

export const getNewProduct = createAsyncThunk(
  "product/newProducts",
  async (data, { rejectWithValue }) => {
    const response = await apiGetProducts({ sort: "-createdAt" });
    if (!response.data.success) return rejectWithValue(response);
    return response.data.products;
  }
);
