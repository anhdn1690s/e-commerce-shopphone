import { createSlice } from "@reduxjs/toolkit/";
import { getNewProduct } from "./asyncActions";

export const productSlice = createSlice({
  name: "product",
  initialState: {
    newProduct: null,
    errorMessage: "",
    isLoading: false,
  },
  reducers: {
    // logout: (state) => {
    //   state.isLoading = false;
    // },
  },
  extraReducers: (builder) => {
    builder.addCase(getNewProduct.pending, (state, action) => {
      state.isLoading = true;
    });
    builder.addCase(getNewProduct.fulfilled, (state, action) => {
      state.isLoading = false;
      state.newProducts = action.payload;
      // console.log("state", state.newProducts);
    });
    builder.addCase(getNewProduct.rejected, (state, action) => {
      state.isLoading = false;
      state.errorMessage = action.payload.messages;
    });
  },
});

export const {} = productSlice.actions;
export default productSlice.reducer;
