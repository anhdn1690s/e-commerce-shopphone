import { createAsyncThunk } from "@reduxjs/toolkit";
import { apiGetCurrent } from "../../api/user";

export const getCurrent = createAsyncThunk(
  "user/current",
  async (data, { rejectWithValue }) => {
    const response = await apiGetCurrent();
    if (!response.data.success) return rejectWithValue(response);
    return response.data.rs;
  }
);
