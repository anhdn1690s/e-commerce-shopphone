import { createSlice } from "@reduxjs/toolkit/";
import { getCurrent } from "./asyncActions";

export const userSlice = createSlice({
  name: "user",
  initialState: {
    isLoggedIn: false,
    current: null,
    token: null,
    isLoading: false,
  },
  reducers: {
    login: (state, actions) => {
      state.isLoggedIn = actions.payload.isLoggedIn;
      state.token = actions.payload.token;
    },
    logout: (state, actions) => {
      state.isLoggedIn = false;
      state.token = null;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(getCurrent.pending, (state, action) => {
      state.isLoading = true;
    });
    builder.addCase(getCurrent.fulfilled, (state, action) => {
      // console.log("action", action);
      state.isLoading = false;
      state.current = action.payload;
      // console.log("state", state.categories);
    });
    builder.addCase(getCurrent.rejected, (state, action) => {
      state.isLoading = false;
      state.current = null;
    });
  },
});

export const { login, logout } = userSlice.actions;
export default userSlice.reducer;
