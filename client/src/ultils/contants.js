import path from "./path";
export const navigation = [
  {
    id: 0,
    value: "HOME",
    path: `/${path.HOME}`,
  },
  {
    id: 1,
    value: "PRODUCT",
    path: `/${path.PRODUCT}`,
  },
  {
    id: 2,
    value: "BLOGS",
    path: `/${path.BLOGS}`,
  },
  {
    id: 3,
    value: "OUR SERVICES",
    path: `/${path.OUR_SERVICES}`,
  },
  {
    id: 4,
    value: "FAQs",
    path: `/${path.FAQS}`,
  },
];
export const color = ["Black", "Gold", "Gray", "White"];
