import icons from "./icons";

const { AiFillStar, AiOutlineStar } = icons;

export const createSlug = (string) => string.toLowerCase().replace(/\s/g, "-");

export const formatMoney = (number) =>
  Number(number?.toFixed(1)).toLocaleString();

export const renderStartFromNumber = (number) => {
  if (!Number(number)) return;
  const starts = [];
  for (let i = 0; i < +number; i++) starts.push(<AiFillStar color="#f1b400" />);
  for (let i = 5; i > +number; i--)
    starts.push(<AiOutlineStar color="#f1b400" />);
  return starts;
};

// export const validate = (payload, setInvalidFields) => {
//   let invalids = 0;
//   const formatPayload = Object.entries(payload);
//   for (let arr of formatPayload) {
//     if (arr[1].trim() === "") {
//       invalids++;
//       setInvalidFields((prev) => [
//         ...prev,
//         { name: arr[0], mes: "Require this field" },
//       ]);
//     }
//   }

//   return invalids;
// };

// for (let arr of formatPayload) {
//   switch (arr[0]) {
//     case "email":
//       const regex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
//       if (!arr[1].match(regex)) {
//         invalids++;
//         setInvalidFields((prev) => [
//           ...prev,
//           { name: arr[0], mes: "Email validate" },
//         ]);
//       }
//       break;
//     case "password":
//       if (arr[1].length < 6) {
//         invalids++;
//         setInvalidFields((prev) => [
//           ...prev,
//           { name: arr[0], mes: "password min 6 " },
//         ]);
//       }
//       break;
//     default:
//       break;
//   }
// }
