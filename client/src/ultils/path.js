const path = {
  PUBLIC: "/",
  HOME: "",
  ALL: "*",
  LOGIN: "login",
  PRODUCT: ":category",
  BLOGS: "blogs",
  OUR_SERVICES: "services",
  FAQS: "faqs",
  FINAL_REGISTER: "finalRegister/:status",
  DETAIL_PRODUCT__CATEGORY__PID__TITLE: ":category/:pid/:title",
  RESET_PASSWORD: "reset-password/:token",
};

export default path;
