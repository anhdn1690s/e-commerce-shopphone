const blogCategory = require("../models/blogCategory");
const asyncHandler = require("express-async-handler");

const createCategoryBlog = asyncHandler(async (req, res) => {
  const response = await blogCategory.create(req.body);
  return res.json({
    success: response ? true : false,
    createCategory: response ? response : "Cannot Create new blog-category",
  });
});

const getCategoryBlog = asyncHandler(async (req, res) => {
  const response = await blogCategory.find().select("title _id");
  return res.json({
    success: response ? true : false,
    blogCategory: response ? response : "Cannot Get blog-category",
  });
});

const updateCategoryBlog = asyncHandler(async (req, res) => {
  const { bcid } = req.params;
  const response = await blogCategory.findByIdAndUpdate(bcid, req.body, {
    new: true,
  });
  return res.json({
    success: response ? true : false,
    updateCategory: response ? response : "Cannot Update blog-category",
  });
});

const deleteCategoryBlog = asyncHandler(async (req, res) => {
  const { bcid } = req.params;
  const response = await blogCategory.findByIdAndDelete(bcid);
  return res.json({
    success: response ? true : false,
    deleteCategory: response ? response : "Cannot Detele blog-category",
  });
});

module.exports = {
    createCategoryBlog,
    getCategoryBlog,
    updateCategoryBlog,
    deleteCategoryBlog,
};
